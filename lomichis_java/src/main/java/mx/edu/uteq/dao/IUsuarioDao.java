/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package mx.edu.uteq.dao;

import java.util.List;
import mx.edu.uteq.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IUsuarioDao extends JpaRepository<Usuario, Long>{
    @Query(value = "SELECT COUNT(*) FROM usuarios WHERE nombre=:nombre and password=:password", nativeQuery = true)
    int findAllActiveUsersNative(@Param("nombre") String nombre, @Param("password") String password);
}
