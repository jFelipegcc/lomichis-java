package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.Usuario;
import mx.edu.uteq.dao.IUsuarioDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class IUsuarioImp implements IUsuarioService{
    @Autowired
    private IUsuarioDao UsuarioDao;
    
    @Override
    @Transactional(readOnly = true)
    public List<Usuario> ListaUsuarios(){
        return (List<Usuario>) UsuarioDao.findAll();
    }

    @Override
    @Transactional   
    public void guardar(Usuario usuario) {
        UsuarioDao.save(usuario);
    }

    @Override
    @Transactional
    public void eliminar(Usuario usuario) {
        UsuarioDao.delete(usuario);
    }

    @Override
    @Transactional(readOnly = true)
    public Usuario findUser(Usuario usuario) {
        return UsuarioDao.findById(usuario.getId()).orElse(null);
    }
    
    
    @Override
    public int selectUsers(Usuario usuario) {
        return UsuarioDao.findAllActiveUsersNative(usuario.getNombre(),usuario.getPassword());
    }

 
    
    
}
