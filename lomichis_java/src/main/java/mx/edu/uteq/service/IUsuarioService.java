/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package mx.edu.uteq.service;

import java.util.List;
import mx.edu.uteq.Usuario;

public interface IUsuarioService {
    public List<Usuario> ListaUsuarios();
    public void guardar(Usuario usuario);
    public void eliminar(Usuario usuario);
    public Usuario findUser(Usuario usuario);
    public int selectUsers(Usuario usuario);
    
}
