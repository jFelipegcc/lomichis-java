
package mx.edu.uteq.controller;

import java.util.List;
import mx.edu.uteq.Usuario;
import mx.edu.uteq.dao.IUsuarioDao;
import mx.edu.uteq.service.IUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class Home {
    @Autowired 
    IUsuarioService usuarioService;
    
    @GetMapping("/home")
    public String page(Model model) {
        return "home";        
    }
    
    @GetMapping("/otra_pagina") //solo queremos  peticiones get
    public String otraPage(Model model) {     
       List<Usuario> datos = usuarioService.ListaUsuarios();
       model.addAttribute ("datos",datos);
       return "otro_view";   
    }
    
    @PostMapping("/validar") 
    public String loginPage(Usuario usuario, Model model) {
        int is = usuarioService.selectUsers(usuario);
        if(is>0){
            return "home";
        }else{
            return "login";
        }   
    }
    
    
    @GetMapping("/login") 
    public String validarPage(Usuario usuario) {   
       return "login";   
    }
    
    
    @GetMapping("/registrate") 
    public String regPage(Usuario usuario) {   
       return "registrate";   
    }
    
    @PostMapping("/crear") 
    public String crearPage(Usuario usuario) {   
        usuarioService.guardar(usuario);
       return "redirect:/otra_pagina";   
    }
    
    
    
}
